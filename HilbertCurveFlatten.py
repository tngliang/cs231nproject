import numpy as np
import tensorflow as tf

class HilbertCurveFlatten(tf.keras.layers.Layer):
  def __init__(self, num_outputs):
    super(HilbertCurveFlatten, self).__init__()
    self.num_outputs = num_outputs

  def build(self, input_shape):
    assert(input_shape[0] == input_shape[1])
    assert(np.prod(input_shape)==self.num_outputs)

  def call(self, input):
    return tf.reshape(input, self.kernel)
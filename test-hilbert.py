import csv
import PIL
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds
from operator import itemgetter
import operator
import pdb

def get_hilbert_curve(filename: str):
    to1D={}
    to2D={}
    indices2D = []
    with open('model_data/{:s}'.format(filename)) as csvfile:
        for row in csv.reader(csvfile, dialect=csv.excel, delimiter=','):
            X, Y, I = int(row[0]), int(row[1]), int(row[2]) 
            indices2D.append((X,Y))
            to1D[(X, Y)] = I
            to2D[I] = (X, Y)
    return np.array(indices2D), to1D, to2D

hilbert_dim = 32
hilbert_indices2D, hilbert_to1D, hilbert_to2D = get_hilbert_curve('HilbertCurvePy{:d}.csv'.format(hilbert_dim))

def preprocess_box(box):
    # y_min, x_min, y_max, x_max => x_min, y_min, x_max, y_max
    pdb.set_trace()
    x, y = tf.meshgrid(tf.range(box[1], box[3]), tf.range(box[0], box[2]), indexing='ij')
    indices = tf.transpose(tf.stack((tf.reshape(x,(-1,)), tf.reshape(y,(-1,)))))
    box_masks = tf.cast(tf.scatter_nd(indices, tf.ones(indices.shape[0]), shape=[32,32]), dtype=tf.int32)
    pdb.set_trace()
    return box_masks

def merge_box_masks(image_buffer):
    def preprocess(image, filename, boxes_extents_yx, boxes_class, boxes_isoccluded):
        nonlocal image_buffer
        image_buffer[str(filename, 'utf-8')] = image
        box_masks = np.zeros([hilbert_dim, hilbert_dim], dtype=int)
        N = boxes_extents_yx.shape[0]
        if N > 0:
            boxes_min_yx = np.floor(boxes_extents_yx[:, 0:2] * hilbert_dim).astype(int)
            boxes_max_yx = np.ceil(boxes_extents_yx[:,2:4] * hilbert_dim).astype(int)
            # REVIEW currently use inefficient pass to mask portions of occluded objects by non-occluded objects
            for i in range(N):
                # y_min, x_min, y_max, x_max => x_min, y_min, x_max, y_max
                col, row = np.meshgrid(np.arange(boxes_min_yx[i,1], boxes_max_yx[i,1]), np.arange(boxes_min_yx[i,0], boxes_max_yx[i,0]))
                box_masks[row, col] = boxes_class[i]
            for i in range(N):
                # y_min, x_min, y_max, x_max => x_min, y_min, x_max, y_max
                col, row = np.meshgrid(np.arange(boxes_min_yx[i,1], boxes_max_yx[i,1]), np.arange(boxes_min_yx[i,0], boxes_max_yx[i,0]))
                if not boxes_isoccluded[i]:
                    box_masks[row, col] = boxes_class[i]
            if (np.sum(box_masks!=0)==0):
                pdb.set_trace()
        return np.int32(box_masks)
    return preprocess

def resize_preprocessor(image_buffer):
    def preprocess(example):
        nonlocal image_buffer
        image, filename, bobjects = example["image"], example["image/filename"], example["bobjects"]
        image_resized_centered = tf.image.resize(image, [hilbert_dim, hilbert_dim]) / 255.
        image_hilbert = tf.gather_nd(image_resized_centered, hilbert_indices2D)
        boxes_classes = bobjects['label']
        boxes_extents_yx = bobjects['bbox']
        boxes_isoccluded = bobjects['is_occluded']
        box_masks = tf.py_func(merge_box_masks(image_buffer), (image, filename, boxes_extents_yx, boxes_classes, boxes_isoccluded), (tf.int32))
        box_masks_hilbert = tf.gather_nd(box_masks, hilbert_indices2D)
        return filename, image_hilbert, box_masks_hilbert
        #return filename, image_resized_centered, box_masks
    return preprocess

open_images_ds = tfds.load(name="open_images_v4", split=tfds.Split.ALL)
assert isinstance(open_images_ds, tf.data.Dataset)
open_images_ds
image_buffer = {}
batch_size = 100
iter = open_images_ds.map(resize_preprocessor(image_buffer)).batch(batch_size).make_one_shot_iterator()
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    filenames, image_data, boxes = sess.run(iter.get_next())
pdb.set_trace()


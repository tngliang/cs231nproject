import csv
import PIL
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds
from yolo_utils import read_classes, read_anchors, generate_colors, preprocess_image, draw_boxes, scale_boxes
from yad2k.models.keras_yolo import yolo_eval, yolo_head, yolo_boxes_to_corners, preprocess_true_boxes, yolo_loss, yolo_body
from operator import itemgetter
import operator
import pdb

categories={}
with open('model_data/class-descriptions-boxable.csv') as csvfile:
    classreader = csv.reader(csvfile, dialect=csv.excel, delimiter=',')
    for row in classreader:
        categories[row[0]] = row[1]
class_names = []
with open('model_data/bobjects-label.labels.txt') as txtfile:
    line = txtfile.readline()
    while line:
        label = line.rstrip('\n')
        class_names.append(label)
        line = txtfile.readline()
class_names = np.array(class_names)
class_names_text = itemgetter(*class_names)(categories)
anchors = read_anchors("model_data/yolo_anchors.txt")

open_images_ds = tfds.load(name="open_images_v4", split=tfds.Split.ALL)
assert isinstance(open_images_ds, tf.data.Dataset)
open_images_ds
iter = open_images_ds.make_one_shot_iterator()
with tf.Session() as sess:
    example = sess.run(iter.get_next())
#example = open_images_ds.take(1)
image, filename, bobjects = example["image"], example["image/filename"], example["bobjects"]

#image_data = tf.image.resize_images(image, [608, 608])
image_data = tf.image.resize(image, [608, 608])
image_data /= 255.0  # normalize to [0,1] range
orig_size = np.array([image.shape[1], image.shape[0]])
orig_size = np.expand_dims(orig_size, axis=0)

image_shape = (image.shape[0], image.shape[1])
#boxes_classes = bobjects['label'].numpy()
#boxes_extents_yx = bobjects['bbox'].numpy()
boxes_classes = bobjects['label']
boxes_extents_yx = bobjects['bbox']
boxes_yx = boxes_extents_yx
class_names_keys = class_names[boxes_classes]
class_names_lookup = operator.itemgetter(*class_names_keys)
#PIL_image = PIL.Image.fromarray(image.numpy())
PIL_image = PIL.Image.fromarray(image)
plt.imshow(PIL_image, interpolation='nearest')
plt.show()
scores = np.ones(len(class_names_keys))
colors = generate_colors(class_names)
boxes_scaled_yx = scale_boxes(boxes_yx, image_shape)
draw_boxes(PIL_image, scores, boxes_scaled_yx, boxes_classes, class_names_text, colors)
plt.imshow(PIL_image, interpolation='nearest')
plt.show()
pdb.set_trace()

# Box preprocessing.
# y_min, x_min, y_max, x_max
# x_min, y_min, x_max, y_max
boxes_extents = boxes_extents_yx[:, [1, 0, 3, 2]]
# Original boxes as 1D list of class, y_min, x_min, y_max, x_max.
boxes_xy = 0.5 * (boxes_extents[:, 0:2] + boxes_extents[:, 2:4])
boxes_wh = boxes_extents[:, 2:4] - boxes_extents[:, 0:2]
boxes = np.concatenate((boxes_xy, boxes_wh, [boxes_classes]), axis=1)
    
# Precompute detectors_mask and matching_true_boxes for training.
# Detectors mask is 1 for each spatial position in the final conv layer and
# anchor that should be active for the given boxes and 0 otherwise.
# Matching true boxes gives the regression targets for the ground truth box
# that caused a detector to be active or 0 otherwise.
detectors_mask_shape = (19, 19, 5, 1)
matching_boxes_shape = (19, 19, 5, 5)
detectors_mask, matching_true_boxes = preprocess_true_boxes(boxes, anchors, [608, 608])
pdb.set_trace()

yolo_body = tf.keras.models.load_model('model_data/yolo.h5')
#yolo_outputs = (box_confidence, box_xy, box_wh, box_class_probs)
yolo_outputs = yolo_head(yolo_body.output, anchors, len(class_names))
scores, boxes, classes = yolo_eval(yolo_outputs, image_shape)
#model = tf.compat.v1.keras.models.load_model('model_data/yolo.h5')
#import json
#with open('yolo_model.json') as f:
#  json_string = f.read()
#model = tf.keras.models.model_from_json(json_string)
#image_file = 'test.jpg'
#tf.compat.v1.disable_eager_execution()
#image, image_data = preprocess_image("images/" + image_file, model_image_size = (608, 608))
#model = yolo_body(image_data, 10, 80)
#json_string = model.to_json()
pdb.set_trace()

class MyModel(tf.keras.Model):
  def __init__(self):
    super(MyModel, self).__init__()
    self.conv1 = tf.keras.layers.Conv2D(32, 3, activation='relu')
    self.flatten = tf.keras.layers.Flatten()
    self.fc1 = tf.keras.layers.Dense(128, activation='relu')
    self.fc2 = tf.keras.layers.Dense(10, activation='softmax')

  def call(self, x):
    x = self.conv1(x)
    x = self.flatten(x)
    x = self.fc1(x)
    return self.fc2(x)

model = MyModel()

loss_object = tf.keras.losses.SparseCategoricalCrossentropy()

optimizer = tf.keras.optimizers.Adam()

train_loss = tf.keras.metrics.Mean(name='train_loss')
train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='train_accuracy')

test_loss = tf.keras.metrics.Mean(name='test_loss')
test_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='test_accuracy')

@tf.function
def train_step(images, labels):
  with tf.GradientTape() as tape:
    predictions = model(images)
    loss = loss_object(labels, predictions)
  gradients = tape.gradient(loss, model.trainable_variables)
  optimizer.apply_gradients(zip(gradients, model.trainable_variables))

  train_loss(loss)
  train_accuracy(labels, predictions)

@tf.function
def test_step(images, labels):
  predictions = model(images)
  t_loss = loss_object(labels, predictions)

  test_loss(t_loss)
  test_accuracy(labels, predictions)

EPOCHS = 5

for epoch in range(EPOCHS):
  for images, labels in train_ds:
    train_step(images, labels)

  for test_images, test_labels in test_ds:
    test_step(test_images, test_labels)

  template = 'Epoch {}, Loss: {}, Accuracy: {}, Test Loss: {}, Test Accuracy: {}'
  print (template.format(epoch+1,
                         train_loss.result(),
                         train_accuracy.result()*100,
                         test_loss.result(),
                         test_accuracy.result()*100))

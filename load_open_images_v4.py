import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds

openimagesv4_train = tfds.load(name="open_images_v4", split=tfds.Split.TRAIN)
assert isinstance(openimagesv4_train, tf.data.Dataset)

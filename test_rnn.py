"""
This is a script that can be used to train Hilbert Curve based RNN model for open image v4 dataset.
"""
import argparse
import csv
from operator import itemgetter
import operator
import os
import matplotlib.pyplot as plt
import numpy as np
import PIL
import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.keras import backend as K
from tensorflow.keras.layers import Input, Conv1D, BatchNormalization, Activation, Dropout, Bidirectional, GRU, TimeDistributed, Dense 
from tensorflow.keras.models import load_model, Model, Sequential
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
from yolo_utils import generate_colors, draw_boxes, scale_boxes

import pdb

# Args
argparser = argparse.ArgumentParser(description="Train a GRU based bi-directional RNN for object detection.")

#argparser.add_argument(
#    '-d',
#    '--data_path',
#    help="path to numpy data file (.npz) containing np.object array 'boxes' and np.uint8 array 'images'",
#    default=os.path.join('..', 'DATA', 'underwater_data.npz'))

#argparser.add_argument(
#    '-c',
#    '--classes_path',
#    help='path to classes file, defaults to pascal_classes.txt',
#    default=os.path.join('..', 'DATA', 'underwater_classes.txt'))

def get_hilbert_curve(filename: str):
    to1D={}
    to2D={}
    indices2D = []
    with open('model_data/{:s}'.format(filename)) as csvfile:
        for row in csv.reader(csvfile, dialect=csv.excel, delimiter=','):
            X, Y, I = int(row[0]), int(row[1]), int(row[2]) 
            indices2D.append((X,Y))
            to1D[(X, Y)] = I
            to2D[I] = (X, Y)
    return np.array(indices2D), to1D, to2D

hilbert_dim = 32
hilbert_indices2D, hilbert_to1D, hilbert_to2D = get_hilbert_curve('HilbertCurvePy{:d}.csv'.format(hilbert_dim))

def merge_box_masks(image_buffer):
    def preprocess(image, filename, boxes_extents_yx, boxes_class, boxes_isoccluded):
        nonlocal image_buffer
        image_buffer[str(filename, 'utf-8')] = image
        box_masks = np.zeros([hilbert_dim, hilbert_dim], dtype=int)
        N = boxes_extents_yx.shape[0]
        if N > 0:
            boxes_min_yx = np.floor(boxes_extents_yx[:, 0:2] * hilbert_dim).astype(int)
            boxes_max_yx = np.ceil(boxes_extents_yx[:,2:4] * hilbert_dim).astype(int)
            # REVIEW currently use inefficient pass to mask portions of occluded objects by non-occluded objects
            for i in range(N):
                # y_min, x_min, y_max, x_max => x_min, y_min, x_max, y_max
                col, row = np.meshgrid(np.arange(boxes_min_yx[i,1], boxes_max_yx[i,1]), np.arange(boxes_min_yx[i,0], boxes_max_yx[i,0]))
                box_masks[row, col] = boxes_class[i]
            for i in range(N):
                # y_min, x_min, y_max, x_max => x_min, y_min, x_max, y_max
                col, row = np.meshgrid(np.arange(boxes_min_yx[i,1], boxes_max_yx[i,1]), np.arange(boxes_min_yx[i,0], boxes_max_yx[i,0]))
                if not boxes_isoccluded[i]:
                    box_masks[row, col] = boxes_class[i]
            #if (np.sum(box_masks!=0)==0): # It is possible when boxes_min_yx is (0, 0) and lead to this.
            #    pdb.set_trace()
        return np.int32(box_masks)
    return preprocess

def resize_preprocessor(image_buffer):
    def preprocess(example):
        nonlocal image_buffer
        image, filename, bobjects = example["image"], example["image/filename"], example["bobjects"]
        image_resized_centered = tf.image.resize(image, [hilbert_dim, hilbert_dim]) / 255.
        image_hilbert = tf.gather_nd(image_resized_centered, hilbert_indices2D)
        boxes_classes = bobjects['label']
        boxes_extents_yx = bobjects['bbox']
        boxes_isoccluded = bobjects['is_occluded']
        box_masks = tf.py_func(merge_box_masks(image_buffer), (image, filename, boxes_extents_yx, boxes_classes, boxes_isoccluded), (tf.int32))
        box_masks_hilbert = tf.expand_dims(tf.gather_nd(box_masks, hilbert_indices2D), axis=1)
        return filename, image, image_hilbert, box_masks_hilbert
        #return filename, image_resized_centered, box_masks
    return preprocess

def create_model(input_shape, num_classes=601, filters=256, kernel_size=1, strides=1, units=128, dropout_rate=0.2):
    '''
    returns the model for object detection using "trigger word detection" 
    # Params:
    # Returns:
    model: Return the Bi-directional GRU based RNN model

    '''
    model = Sequential([
        Conv1D(filters=filters, kernel_size=kernel_size, strides=strides, input_shape=input_shape),
        BatchNormalization(),
        Activation('relu'),
        Dropout(rate=dropout_rate),
        Bidirectional(GRU(units=units, return_sequences=True)),
        Dropout(rate=dropout_rate),
        BatchNormalization(),
        Bidirectional(GRU(units=units, return_sequences=True)),
        Dropout(rate=dropout_rate),
        BatchNormalization(),
        Dropout(rate=dropout_rate),
        TimeDistributed(Dense(num_classes, activation="softmax"))
    ])
    return model

def _main(args):
    #data_path = os.path.expanduser(args.data_path)
    #classes_path = os.path.expanduser(args.classes_path)
    class_names = get_classes()
    
    image_buffer = {}
    train_ds = tfds.load(name="open_images_v4", split=tfds.Split.TEST).map(resize_preprocessor(image_buffer))
    assert isinstance(train_ds, tf.data.Dataset)
    
    batch_size = 1
    #filenames, image_data, boxes = open_images_ds.take(1)
    sess = K.get_session()
    sess.run(tf.global_variables_initializer())
    iter = train_ds.batch(batch_size).make_one_shot_iterator()
    filenames, image, image_data, boxes = sess.run(iter.get_next())
    
    model = create_model([hilbert_dim * hilbert_dim, 3]) # RGB image flattened along Hilbert Curve
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=["sparse_categorical_accuracy"])

    TestByOverfit = False
    #pdb.set_trace()
    if TestByOverfit:
        weights_filename = 'trained_overfit_rnn_{:d}.h5'.format(batch_size)
        
        #train_overfit(weights_filename, num_steps, model, class_names, image_data, boxes)

        draw(model, class_names, image_data, filenames, image_set='all', weights_name=weights_filename, save_all=False)
    else:
        if os.path.exists('trained_stage_3_rnn_best.h5'):
            model.load_weights('trained_stage_3_rnn_best.h5')
            print("Loaded weights from {:s}".format('trained_stage_3_rnn_best.h5'))
        model2 = create_model([hilbert_dim * hilbert_dim, 3]) # RGB image flattened along Hilbert Curve
        model2.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=["sparse_categorical_accuracy"])

        out_path = "rnn_images"
        if  not os.path.exists(out_path):
            os.makedirs(out_path)
        while (len(filenames) > 0):
            #try:
            #train(model, model2, class_names, image_data, boxes)
            #except:
            pdb.set_trace()
            hilbert_map = model.predict(x=image_data)
            for i in range(hilbert_map.shape[0]):
                start = -1
                probs = []
                prediction = []
                for t in range(hilbert_map.shape[1]):
                    prob_vec = np.exp(hilbert_map[i][t])/np.sum(np.exp(hilbert_map[i][t]))
                    probs.append(prob_vec)
                    value = np.argmax(prob_vec)
                    prediction.append(np.argmax(hilbert_map[i][t]))
                    if start == -1:
                        start = t
                    else:
                        if prediction[start] != value:
                            idx = (start + t) // 2
                            bounding_box_center_indices = hilbert_to2D[idx]
                            PIL_image = PIL.Image.fromarray(image[0])
                            out_classes = prediction[idx]
                            out_scores = probs[idx][out_classes]
                            bounding_box_center_yx = np.floor(np.array(bounding_box_center_indices)/hilbert_dim * image.shape[1:2])
                            out_boxes = (max(bounding_box_center_yx[0] - 304, 0), max(bounding_box_center_yx[1] - 304, 0), min(bounding_box_center_yx[0] + 304, image.shape[1]), min(bounding_box_center_yx[1] + 304, image.shape[2]))
                            colors = generate_colors(class_names)
                            draw_boxes(PIL_image, [out_scores], [out_boxes], [out_classes], class_names, colors)
                            filename = str(filenames[0], 'utf-8')
                            pdb.set_trace()
                            PIL_image.save(os.path.join(out_path,filename),format='JPEG')
                            start = t
                pdb.set_trace()
            #draw(model, class_names, image_data, filenames,
            #    image_set='val', # assumes training/validation split is 0.9
            #    weights_name='trained_stage_3_rnn_best.h5',
            #    save_all=False)

            filenames, image, image_data, boxes = sess.run(iter.get_next())

def get_classes():
    '''loads the classes'''
    categories={}
    with open('model_data/class-descriptions-boxable.csv') as csvfile:
        classreader = csv.reader(csvfile, dialect=csv.excel, delimiter=',')
        for row in classreader:
            categories[row[0]] = row[1]
    class_names = []
    with open('model_data/bobjects-label.labels.txt') as txtfile:
        line = txtfile.readline()
        while line:
            label = line.rstrip('\n')
            class_names.append(label)
            line = txtfile.readline()
    class_names = np.array(class_names)
    class_names_text = itemgetter(*class_names)(categories)
    return np.array(class_names_text)

def train_overfit(weights_filename, num_steps, model, class_names, image_data, boxes, validation_split=0.1):
    '''
    test the model by overfitting to a single image
    logs training with tensorboard
    saves training weights in current directory
    best weights according to val_loss is saved as weights_filename
    '''
    logging = TensorBoard()
    model.fit(x=image_data, y=boxes, batch_size=min(len(image_data), 8), epochs=num_steps, callbacks=[logging])
    model.save_weights(weights_filename)
    pdb.set_trace()
    
def train(model, model2, class_names, image_data, boxes, validation_split=0.1):
    '''
    train the model
    logs training with tensorboard
    saves training weights in current directory
    best weights according to val_loss is saved as trained_stage_3_rnn_best.h5
    '''
    logging = TensorBoard()
    checkpoint = ModelCheckpoint('trained_stage_3_rnn_best.h5', monitor='val_loss', save_weights_only=True, save_best_only=True)
    early_stopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=15, verbose=1, mode='auto')

    model.fit(x=image_data, y=boxes, validation_split=validation_split, batch_size=8, epochs=5, callbacks=[logging])
    model.save_weights('trained_stage_1_rnn.h5')

    model2.load_weights('trained_stage_1_rnn.h5')
    model2.fit(x=image_data, y=boxes, validation_split=0.1, batch_size=8, epochs=30, callbacks=[logging])
    model2.save_weights('trained_stage_2_rnn.h5')

    model2.fit(x=image_data, y=boxes, validation_split=0.1, batch_size=8, epochs=30, callbacks=[logging, checkpoint, early_stopping])
    model2.save_weights('trained_stage_3_rnn.h5')

def draw(model, class_names, image_data, filenames, image_set='val', weights_name='trained_stage_3_rnn_best.h5', out_path="rnn_output_images", save_all=True):
    '''
    Draw bounding boxes on image data
    '''
    if image_set == 'train':
        image_data = np.array([np.expand_dims(image, axis=0) for image in image_data[:int(len(image_data)*.9)]])
    elif image_set == 'val':
        image_data = np.array([np.expand_dims(image, axis=0) for image in image_data[int(len(image_data)*.9):]])
    elif image_set == 'all':
        image_data = np.array([np.expand_dims(image, axis=0) for image in image_data])
    else:
        ValueError("draw argument image_set must be 'train', 'val', or 'all'")

    print(image_data.shape)
    model.load_weights(weights_name)

    # Run prediction on overfit image.
    sess = K.get_session()  # TODO: Remove dependence on Tensorflow session.

    colors = generate_colors(class_names)
    
    if  not os.path.exists(out_path):
        os.makedirs(out_path)
    for i in range(len(image_data)):
        out_boxes = model.predict(image_data)
        print('Found {} boxes for image.'.format(len(out_boxes)))
        print(out_boxes)
        pdb.set_trace()
        filename = str(filenames[i], 'utf-8')
        try:
            if (filename in image_buffer and len(out_boxes) > 0):
                # Plot image with predicted boxes.
                PIL_image = PIL.Image.fromarray(image_buffer[filename])
                #image_with_boxes = draw_boxes(image_data[i][0], out_boxes, out_classes, class_names, out_scores)
                draw_boxes(PIL_image, out_scores, out_boxes, out_classes, class_names, colors)
                #PIL_image = PIL.Image.frombytes(mode="RGB",size=[608,608],data=(image_data[i][0] * 255.).astype(int))
                # Save the image:
                if save_all or (len(out_boxes) > 0):
                    #image = PIL.Image.fromarray(image_with_boxes)
                    PIL_image.save(os.path.join(out_path,filename),format='JPEG')

                # To display (pauses the program):
                # plt.imshow(image_with_boxes, interpolation='nearest')
                # plt.show()
        except:
            pdb.set_trace()

if __name__ == '__main__':
    args = argparser.parse_args()
    _main(args)

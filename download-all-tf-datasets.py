import argparse
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds

DATASET_FILES = [
    'image/imagenet2012_labels.txt',
    'image/imagenet2012_validation_labels.txt',
    'image/quickdraw_labels.txt',
    'open_images_classes_all.txt',
    'open_images_classes_boxable.txt',
    'open_images_classes_trainable.txt',
    'url_checksums/*',
]
def main(args):
  if args.name is None:
    for dataset_name in tfds.list_builders():
      try:
        print("downloading {:s}".format(dataset_name))
        all = tfds.load(name=dataset_name, split=tfds.Split.ALL)
        assert isinstance(all, tf.data.Dataset)
      except:
        print("downloading {:s} failed".format(dataset_name))
  else:
    try:
      print("downloading {:s}".format(args.name))
      all = tfds.load(name=args.name, split=tfds.Split.ALL)
      assert isinstance(all, tf.data.Dataset)
    except:
      print("downloading {:s} failed".format(args.name))


if __name__ == '__main__':
  parser = argparse.ArgumentParser('Download tf dataset')
  parser.add_argument('-n', '--name', help='name of dataset')
  args = parser.parse_args()
  main(args)
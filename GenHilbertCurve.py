import numpy as np 
import h5py as h5py
from hilbertcurve.hilbertcurve import HilbertCurve
#p=1
#N=2
#hilbert_curve = HilbertCurve(p, N)

#for ii in range(4):
#    coords = hilbert_curve.coordinates_from_distance(ii)
#    print(f'coords(h={ii}) = {coords}')

#for coords in [[0,0], [0,1], [1,1], [1,0]]:
#    dist = hilbert_curve.distance_from_coordinates(coords)
#    print(f'distance(x={coords}) = {dist}')

p=5
N=2
shape=(N**p,N**p)
hilbert_curve = HilbertCurve(p, N)
for ii in range(np.prod(shape)):
    coords = hilbert_curve.coordinates_from_distance(ii)
    print('{:d},{:d},{:d}'.format(coords[0],coords[1],ii))
#x, y = np.meshgrid(range(19), range(19))
#xy = np.array([x.flatten(),y.flatten()]).T
#for coords in xy:
#    dist = hilbert_curve.distance_from_coordinates(coords)
#    print(f'distance(x={coords}) = {dist}')

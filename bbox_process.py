import csv
import json
import pdb

categories={}
with open('model_data/class-descriptions.csv') as csvfile:
    classreader = csv.reader(csvfile, dialect=csv.excel, delimiter=',')
    for row in classreader:
        categories[row[0]] = row[1]

def printCategory(data: dict, depth: int):
    if 'LabelName' in data:
        raw = data['LabelName']
        if raw in categories:
            print(categories[raw])
        else:
            print(raw)
    if 'Subcategory' in data:
        for category in data['Subcategory']:
            printCategory(category, depth+1)

lineCount = 0
with open('model_data/bobjects-label.labels.txt') as txtfile:
    line = txtfile.readline()
    while line:
        label = line.rstrip('\n')
        print('{:d} {:s}={:s}'.format(lineCount, label, categories[label]))
        line = txtfile.readline()
        lineCount += 1
	
with open("model_data/bbox_labels_600_hierarchy.json", "r") as read_file:
    data = json.load(read_file)
    printCategory(data, 0)
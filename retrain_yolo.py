"""
This is a script that can be used to retrain the YOLOv2 model for your own dataset.
"""
import argparse
import csv
from operator import itemgetter
import operator
import os

import matplotlib.pyplot as plt
import numpy as np
import PIL
import tensorflow as tf
import tensorflow_datasets as tfds
from keras import backend as K
from keras.layers import Input, Lambda, Conv2D
from keras.models import load_model, Model
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping

from yad2k.models.keras_darknet19 import (DarknetConv2D, DarknetConv2D_BN_Leaky, darknet_body)
from yad2k.models.keras_yolo import (preprocess_true_boxes, yolo_body,
                                     yolo_eval, yolo_head, yolo_loss)
from yad2k.utils.draw_boxes import draw_boxes
from yolo_utils import read_classes, read_anchors, generate_colors, preprocess_image, draw_boxes, scale_boxes
import pdb

run_options = tf.RunOptions(report_tensor_allocations_upon_oom = False)

# Args
argparser = argparse.ArgumentParser(
    description="Retrain or 'fine-tune' a pretrained YOLOv2 model for your own data.")

argparser.add_argument(
    '-d',
    '--data_path',
    help="path to numpy data file (.npz) containing np.object array 'boxes' and np.uint8 array 'images'",
    default=os.path.join('..', 'DATA', 'underwater_data.npz'))

argparser.add_argument(
    '-a',
    '--anchors_path',
    help='path to anchors file, defaults to yolo_anchors.txt',
    default=os.path.join('model_data', 'yolo_anchors.txt'))

argparser.add_argument(
    '-c',
    '--classes_path',
    help='path to classes file, defaults to pascal_classes.txt',
    default=os.path.join('..', 'DATA', 'underwater_classes.txt'))

# Default anchor boxes
YOLO_ANCHORS = np.array(
    ((0.57273, 0.677385), (1.87446, 2.06253), (3.33843, 5.47434),
     (7.88282, 3.52778), (9.77052, 9.16828)))

max_boxes = 500
#max_boxes = 1

def preprocess_max_true_boxes(image_buffer):
    def preprocess(image, filename, boxes, anchors, image_size):
        nonlocal image_buffer
        image_buffer[str(filename, 'utf-8')] = image
        detectors_mask, matching_true_boxes = preprocess_true_boxes(boxes, anchors, image_size)
        if len(boxes.shape) < 2:
            boxes = np.expand_dims(boxes, axis=0)
        # add zero pad for training
        if boxes.shape[0] <= max_boxes:
            boxes = np.vstack((boxes, np.zeros((max_boxes - boxes.shape[0], 5), dtype=np.float32)))
        else:
            pdb.set_trace()
        return boxes, detectors_mask, matching_true_boxes
    return preprocess

def resize_preprocessor(image_buffer):
    def preprocess(example):
        nonlocal image_buffer
        image, filename, bobjects = example["image"], example["image/filename"], example["bobjects"]
        image_resized_centered = tf.image.resize(image, [608, 608]) / 255.
        boxes_classes = bobjects['label']
        boxes_extents_yx = bobjects['bbox']
        # y_min, x_min, y_max, x_max => x_min, y_min, x_max, y_max
        boxes_extents_yx_t = tf.transpose(boxes_extents_yx)
        boxes_lt = tf.transpose(tf.gather(boxes_extents_yx_t, [1,0]))
        boxes_rb = tf.transpose(tf.gather(boxes_extents_yx_t, [3,2]))
        # Original boxes as 1D list of class, y_min, x_min, y_max, x_max.
        boxes_xy = 0.5 * (boxes_lt + boxes_rb)
        boxes_wh = boxes_rb - boxes_lt
        boxes = tf.concat([boxes_xy, boxes_wh, tf.reshape(tf.cast(boxes_classes, tf.float32), (-1,1))], axis=1)
        preprocessor = preprocess_max_true_boxes(image_buffer)
        boxes_zero_padded, detectors_mask, matching_true_boxes = tf.py_func(preprocessor, [image, filename, boxes, YOLO_ANCHORS, [608, 608]], (tf.float32, tf.float32, tf.float32))
        return (filename, image_resized_centered, boxes_zero_padded, detectors_mask, matching_true_boxes)
    return preprocess
        
def _main(args):
    data_path = os.path.expanduser(args.data_path)
    classes_path = os.path.expanduser(args.classes_path)
    anchors_path = os.path.expanduser(args.anchors_path)

    class_names = get_classes(classes_path)
    anchors = get_anchors(anchors_path)

    #data = np.load(data_path) # custom data saved as a numpy file.
    #  has 2 arrays: an object array 'boxes' (variable length of boxes in each image)
    #  and an array of images 'images'

    #image_data, boxes = process_data(data['images'], data['boxes'])
    #train_ds = tfds.load(name="open_images_v4", split=tfds.Split.TRAIN)
    train_ds = tfds.load(name="open_images_v4", split=tfds.Split.ALL)
    image_buffer = {}
    resize = resize_preprocessor(image_buffer)
    train_ds_resized = train_ds.map(resize)

    yolo_model, model = create_model(anchors, class_names)
    model.compile(optimizer='adam', loss={'yolo_loss': lambda y_true, y_pred: y_pred})  # This is a hack to use the custom loss function in the last layer.

    # Create output variables for prediction.
    yolo_outputs = yolo_head(yolo_model.output, anchors, len(class_names))
    input_image_shape = K.placeholder(shape=(2, ))
    #eval_boxes, eval_scores, eval_classes = yolo_eval(yolo_outputs, input_image_shape, score_threshold=0.07, iou_threshold=0.)
    eval_boxes, eval_scores, eval_classes = yolo_eval(yolo_outputs, input_image_shape, score_threshold=0.3, iou_threshold=0.9)

    _, model2 = create_model(anchors, class_names, load_pretrained=False, freeze_body=False)
    model2.compile(optimizer='adam', loss={'yolo_loss': lambda y_true, y_pred: y_pred})  # This is a hack to use the custom loss function in the last layer.

    batch_size=1000
    #batch_size=8
    num_steps=1000
    iter = train_ds_resized.shuffle(128).batch(batch_size).make_one_shot_iterator()
    #iter = train_ds_resized.batch(batch_size).make_one_shot_iterator()
    sess = K.get_session()
    sess.run(tf.global_variables_initializer(), options=run_options)
    #example, = open_images_ds.take(1)
    filenames, image_data, boxes, detectors_mask, matching_true_boxes = sess.run(iter.get_next(), options=run_options)
    for filename in filenames:
        assert(str(filename, 'utf-8') in image_buffer)
    
    TestByOverfit = False
    pdb.set_trace()
    if TestByOverfit:
        weights_filename = 'trained_overfit_{:d}_{:d}.h5'.format(batch_size, num_steps)

        train_overfit(
            weights_filename,
            num_steps,
            yolo_model,
            model,
            class_names,
            anchors,
            image_data,
            boxes,
            detectors_mask,
            matching_true_boxes
        )

        yolo_model.load_weights(weights_filename)

        draw(yolo_model, input_image_shape, eval_boxes, eval_scores, eval_classes,
            class_names,
            anchors,
            image_data,
            filenames,
            image_buffer,
            image_set='all',
            save_all=False)
    else:
        while (len(filenames) > 0):
            #try:
            train(
                model,
                model2,
                class_names,
                anchors,
                image_data,
                boxes,
                detectors_mask,
                matching_true_boxes
            )
            #except:
            #    pdb.set_trace()
            yolo_model.load_weights('trained_stage_3_best.h5')
            
            draw(yolo_model, input_image_shape, eval_boxes, eval_scores, eval_classes,
                class_names,
                anchors,
                image_data,
                filenames,
                image_buffer,
                image_set='val', # assumes training/validation split is 0.9
                save_all=False)

            #image_buffer.clear()
            #sess.close()
            #sess = tf.Session()
            #K.set_session(sess)
            #sess.run(tf.global_variables_initializer(), options=run_options)
            filenames, image_data, boxes, detectors_mask, matching_true_boxes = sess.run(iter.get_next(), options=run_options)
            for filename in filenames:
                assert(str(filename, 'utf-8') in image_buffer)

def get_classes(classes_path):
    '''loads the classes'''
    #with open(classes_path) as f:
    #    class_names = f.readlines()
    #class_names = [c.strip() for c in class_names]
    #return class_names
    categories={}
    with open('model_data/class-descriptions-boxable.csv') as csvfile:
        classreader = csv.reader(csvfile, dialect=csv.excel, delimiter=',')
        for row in classreader:
            categories[row[0]] = row[1]
    class_names = []
    with open('model_data/bobjects-label.labels.txt') as txtfile:
        line = txtfile.readline()
        while line:
            label = line.rstrip('\n')
            class_names.append(label)
            line = txtfile.readline()
    class_names = np.array(class_names)
    class_names_text = itemgetter(*class_names)(categories)
    return np.array(class_names_text)

def get_anchors(anchors_path):
    '''loads the anchors from a file'''
    #if os.path.isfile(anchors_path):
    #    with open(anchors_path) as f:
    #        anchors = f.readline()
    #        anchors = [float(x) for x in anchors.split(',')]
    #        return np.array(anchors).reshape(-1, 2)
    #else:
    #    Warning("Could not open anchors file, using default.")
    #    return YOLO_ANCHORS
    anchors = read_anchors("model_data/yolo_anchors.txt")
    return anchors

def process_data(images, boxes=None):
    '''processes the data'''
    images = [PIL.Image.fromarray(i) for i in images]
    orig_size = np.array([images[0].width, images[0].height])
    orig_size = np.expand_dims(orig_size, axis=0)

    # Image preprocessing.
    processed_images = [i.resize((416, 416), PIL.Image.BICUBIC) for i in images]
    processed_images = [np.array(image, dtype=np.float) for image in processed_images]
    processed_images = [image/255. for image in processed_images]

    if boxes is not None:
        # Box preprocessing.
        # Original boxes stored as 1D list of class, x_min, y_min, x_max, y_max.
        boxes = [box.reshape((-1, 5)) for box in boxes]
        # Get extents as y_min, x_min, y_max, x_max, class for comparision with
        # model output.
        boxes_extents = [box[:, [2, 1, 4, 3, 0]] for box in boxes]

        # Get box parameters as x_center, y_center, box_width, box_height, class.
        boxes_xy = [0.5 * (box[:, 3:5] + box[:, 1:3]) for box in boxes]
        boxes_wh = [box[:, 3:5] - box[:, 1:3] for box in boxes]
        boxes_xy = [boxxy / orig_size for boxxy in boxes_xy]
        boxes_wh = [boxwh / orig_size for boxwh in boxes_wh]
        boxes = [np.concatenate((boxes_xy[i], boxes_wh[i], box[:, 0:1]), axis=1) for i, box in enumerate(boxes)]

        # find the max number of boxes
        max_boxes = 0
        for boxz in boxes:
            if boxz.shape[0] > max_boxes:
                max_boxes = boxz.shape[0]

        # add zero pad for training
        for i, boxz in enumerate(boxes):
            if boxz.shape[0]  < max_boxes:
                zero_padding = np.zeros( (max_boxes-boxz.shape[0], 5), dtype=np.float32)
                boxes[i] = np.vstack((boxz, zero_padding))

        return np.array(processed_images), np.array(boxes)
    else:
        return np.array(processed_images)

def get_detector_mask(boxes, anchors):
    '''
    Precompute detectors_mask and matching_true_boxes for training.
    Detectors mask is 1 for each spatial position in the final conv layer and
    anchor that should be active for the given boxes and 0 otherwise.
    Matching true boxes gives the regression targets for the ground truth box
    that caused a detector to be active or 0 otherwise.
    '''
    detectors_mask = [0 for i in range(len(boxes))]
    matching_true_boxes = [0 for i in range(len(boxes))]
    for i, box in enumerate(boxes):
        detectors_mask[i], matching_true_boxes[i] = preprocess_true_boxes(box, anchors, [416, 416])

    return np.array(detectors_mask), np.array(matching_true_boxes)

def create_model(anchors, class_names, load_pretrained=True, freeze_body=True):
    '''
    returns the body of the model and the model

    # Params:

    load_pretrained: whether or not to load the pretrained model or initialize all weights

    freeze_body: whether or not to freeze all weights except for the last layer's

    # Returns:

    model_body: YOLOv2 with new output layer

    model: YOLOv2 with custom loss Lambda layer

    '''

    #detectors_mask_shape = (13, 13, 5, 1)
    #matching_boxes_shape = (13, 13, 5, 5)
    detectors_mask_shape = (19, 19, 5, 1)
    matching_boxes_shape = (19, 19, 5, 5)
    # Create model input layers.
    #image_input = Input(shape=(416, 416, 3))
    image_input = Input(shape=(608, 608, 3))
    boxes_input = Input(shape=(None, 5))
    detectors_mask_input = Input(shape=detectors_mask_shape)
    matching_boxes_input = Input(shape=matching_boxes_shape)

    # Create model body.
    model_body = yolo_body(image_input, len(anchors), len(class_names))
    topless_yolo = Model(model_body.input, model_body.layers[-2].output)
    #model_body = load_model('model_data/yolo.h5')
    #topless_yolo = Model(model_body.input, model_body.layers[-2].output)
    #pdb.set_trace()
    if load_pretrained:
        # Save topless yolo:
        topless_yolo_path = os.path.join('model_data', 'yolo_topless.h5')
        if not os.path.exists(topless_yolo_path):
            print("CREATING TOPLESS WEIGHTS FILE")
            yolo_path = os.path.join('model_data', 'yolo.h5')
            model = load_model(yolo_path)
            topless_model = Model(model.inputs, model.layers[-2].output)
            topless_model.save_weights(topless_yolo_path)
        topless_yolo.load_weights(topless_yolo_path)

    if freeze_body:
        #for layer in topless_yolo.layers:
        #    layer.trainable = False
        for layer in topless_yolo.layers:
            layer.trainable = False
            #if layer.name.startswith('batch_normalization'):
            #    layer.trainable = True
            #else:
                #pdb.set_trace()
                #layer.gamma = K.get_session().run(layer.gamma_initializer(shape=layer.gamma.shape))
                #layer.beta = K.get_session().run(layer.beta_initializer(shape=layer.beta.shape))
                #layer.moving_moving_mean = K.get_session().run(layer.moving_mean_initializer(shape=layer.moving_mean.shape))
                #layer.moving_variance = K.get_session().run(layer.moving_variance_initializer(shape=layer.moving_variance.shape))
    last = model_body.layers.pop()
    final_layer = DarknetConv2D(len(anchors)*(5+len(class_names)), (1, 1), activation='linear')
    final_layer.name = last.name
    yolo_model = Model(topless_yolo.input, final_layer(topless_yolo.output))
    #num_anchors = len(anchors)
    #num_classes = len(class_names)
    #last = model_body.layers.pop()
    #replacement = DarknetConv2D(num_anchors * (num_classes + 5), (1, 1), activation='linear')
    #replacement.name = last.name
    #yolo_model = Model(inputs=model_body.input, outputs=replacement(model_body.layers[-1].output))

    #pdb.set_trace() # opportunity to load trained_stage_3_best.h5
    # Place model loss on CPU to reduce GPU memory usage.
    with tf.device('/cpu:0'):
        # TODO: Replace Lambda with custom Keras layer for loss.
        model_loss = Lambda(
            yolo_loss,
            output_shape=(1, ),
            name='yolo_loss',
            arguments={'anchors': anchors,
                       'num_classes': len(class_names)})([
                           yolo_model.output, boxes_input,
                           detectors_mask_input, matching_boxes_input
                       ])

    model = Model([yolo_model.input, boxes_input, detectors_mask_input, matching_boxes_input], model_loss)
    return yolo_model, model

def train_overfit(weights_filename, num_steps, topless_yolo, model, class_names, anchors, image_data, boxes, detectors_mask, matching_true_boxes, validation_split=0.1):
    '''
    retrain/fine-tune the model

    logs training with tensorboard

    saves training weights in current directory

    best weights according to val_loss is saved as trained_stage_3_best.h5
    '''
    logging = TensorBoard()
    model.fit([image_data, boxes, detectors_mask, matching_true_boxes],
              np.zeros(len(image_data)),
              batch_size=min(len(image_data), 8),
              epochs=num_steps,
              callbacks=[logging])
    model.save_weights(weights_filename)
    pdb.set_trace()
    
def train(model, model2, class_names, anchors, image_data, boxes, detectors_mask, matching_true_boxes, validation_split=0.1):
    '''
    retrain/fine-tune the model

    logs training with tensorboard

    saves training weights in current directory

    best weights according to val_loss is saved as trained_stage_3_best.h5
    '''
    if os.path.exists('trained_stage_3_best.h5'):
        model.load_weights('trained_stage_3_best.h5')

    logging = TensorBoard()
    checkpoint = ModelCheckpoint('trained_stage_3_best.h5', monitor='val_loss', save_weights_only=True, save_best_only=True)
    early_stopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=15, verbose=1, mode='auto')

    model.fit([image_data, boxes, detectors_mask, matching_true_boxes],
              np.zeros(len(image_data)),
              validation_split=validation_split,
              batch_size=8,
              epochs=5,
              callbacks=[logging])
    model.save_weights('trained_stage_1.h5')

    model2.load_weights('trained_stage_1.h5')

    model2.fit([image_data, boxes, detectors_mask, matching_true_boxes],
              np.zeros(len(image_data)),
              validation_split=0.1,
              batch_size=8,
              epochs=30,
              callbacks=[logging])
    model2.save_weights('trained_stage_2.h5')

    model2.fit([image_data, boxes, detectors_mask, matching_true_boxes],
              np.zeros(len(image_data)),
              validation_split=0.1,
              batch_size=8,
              epochs=30,
              callbacks=[logging, checkpoint, early_stopping])
    model2.save_weights('trained_stage_3.h5')

def draw(yolo_model, input_image_shape, boxes, scores, classes, class_names, anchors, image_data, filenames, image_buffer, image_set='val', out_path="output_images", save_all=True):
    '''
    Draw bounding boxes on image data
    '''
    if image_set == 'train':
        image_data = np.array([np.expand_dims(image, axis=0)
            for image in image_data[:int(len(image_data)*.9)]])
    elif image_set == 'val':
        image_data = np.array([np.expand_dims(image, axis=0)
            for image in image_data[int(len(image_data)*.9):]])
    elif image_set == 'all':
        image_data = np.array([np.expand_dims(image, axis=0)
            for image in image_data])
    else:
        ValueError("draw argument image_set must be 'train', 'val', or 'all'")

    print(image_data.shape)

    # Run prediction on overfit image.
    sess = K.get_session()  # TODO: Remove dependence on Tensorflow session.

    colors = generate_colors(class_names)
    
    if  not os.path.exists(out_path):
        os.makedirs(out_path)
    for i in range(len(image_data)):
        out_boxes, out_scores, out_classes = sess.run(
            [boxes, scores, classes],
            feed_dict={
                yolo_model.input: image_data[i],
                input_image_shape: [image_data.shape[2], image_data.shape[3]],
                K.learning_phase(): 0
            }, options=run_options)
        print('Found {} boxes for image.'.format(len(out_boxes)))
        print(out_boxes)
        filename = str(filenames[i], 'utf-8')
        try:
            if (filename in image_buffer and len(out_boxes) > 0):
                # Plot image with predicted boxes.
                PIL_image = PIL.Image.fromarray(image_buffer[filename])
                #image_with_boxes = draw_boxes(image_data[i][0], out_boxes, out_classes, class_names, out_scores)
                draw_boxes(PIL_image, out_scores, out_boxes, out_classes, class_names, colors)
                #PIL_image = PIL.Image.frombytes(mode="RGB",size=[608,608],data=(image_data[i][0] * 255.).astype(int))
                # Save the image:
                if save_all or (len(out_boxes) > 0):
                    #image = PIL.Image.fromarray(image_with_boxes)
                    PIL_image.save(os.path.join(out_path,filename),format='JPEG')

                # To display (pauses the program):
                # plt.imshow(image_with_boxes, interpolation='nearest')
                # plt.show()
        except:
            pdb.set_trace()

if __name__ == '__main__':
    args = argparser.parse_args()
    _main(args)

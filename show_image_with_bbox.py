import tensorflow as tf
from PIL import Image
import matplotlib.pyplot as plt
import pathlib
import pdb

data_root = tf.keras.utils.get_file(origin='https://requestor-proxy.figure-eight.com/figure_eight_datasets/open-images/train/', fname='a61e37a5e7b7a54f.jpg')
img_path = pathlib.Path(data_root)
img_raw = tf.io.read_file(str(img_path))
img_tensor = tf.image.decode_image(img_raw)
pdb.set_trace()
plt.figure()
plt.imshow(img)
plt.show()
